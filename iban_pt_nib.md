# About this source

- Published at GIST, [here](https://gist.github.com/ed970d4306d6e824d29a9e9e136be654.git)


# IBAN NIB part

## Portugal

Responsible entity:
- [BANCO DE PORTUGAL](http://www.bportugal.pt/) -- www.bportugal.pt
- IBAN: [IBAN description](https://www.bportugal.pt/page/iban)
  + [NIB - pdf](https://www.bportugal.pt/sites/default/files/anexos/instrucoes/17-98i2.pdf)

# Credits

Thanks to [gist.github](https://gist.github.com/) for hosting GIST snippets.
This snippet is just a redirection to what 'Banco de Portugal' published.

--
Henrique Moreira
